#!/bin/bash

DATADIR='../../../DataSets/ProjectGutemberg/GERMAN/clean'
    
file1="$DATADIR/pg7276-GERMAN-Hamlet-Shakespeare_clean.txt"
file2="$DATADIR/pg6996-GERMAN-RomeaUndJulia-Shakespeare_clean.txt"
file3="$DATADIR/52012-0-GERMAN-ShakespeareVolume1-Landauer_clean.txt"
#file1="../../../DataSets/ProjectGutemberg/ENGLISH/clean/Shakespeare/pg1112-TheTragedyOfRomeoAndJuliet-Shakespeare_clean.txt"
for alg in ../classic_*.py; do
    tmp=${alg:3}
    tmp=${tmp/.py/.txt}
    python $alg $file1 $file2 "word" >> $tmp
done
for alg in ../adaptive_*.py; do
    tmp=${alg:3}
    tmp=${tmp/.py/.txt}
    python $alg $file1 $file2 "word" >> $tmp
done    
