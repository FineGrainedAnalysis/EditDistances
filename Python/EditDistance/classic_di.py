from ed_algorithms.DPBase import DPBase
from ed_algorithms.StringClassic import StringClassic
from ed_algorithms.WordJump import WordJump
import time

def classic_delete_insert(base, i, j, source, target):
    val = 0
    base.increase_counter()
    if j < 0:
        return i+1
    elif i < 0:
        return j+1
    if base.get(i, j) < 0:
        if source.get(i) == target.get(j):
            val = classic_delete_insert(base, i-1, j-1, source, target)
        else:
            delete_dist = classic_delete_insert(base, i-1, j, source, target)
            insert_dist = classic_delete_insert(base, i, j-1, source, target)
            val = 1 + min(insert_dist, delete_dist)
        base.put(i, j, val)
    return base.get(i, j)


def main(source, target, by_word=False, is_file=False):

    dp = DPBase(classic_delete_insert)
    s = None
    t = None
    if by_word:
        s = WordJump(source, is_file)
        t = WordJump(target, is_file)
    else:
        s = StringClassic(source, is_file)
        t = StringClassic(target, is_file)
    start = time.time()   
    val = dp.run(s, t)
    alg_time = time.time()-start
    if is_file:
        print "Alfabeto s, t: ", len(s.get_alphabet()), len(t.get_alphabet())
        print "Tamano interseccion de alfabeto: ", len((s.get_alphabet() & t.get_alphabet()))
        print "Numero de recursiones: ", dp.get_counter()
        print "Tiempo de algoritmo: ", alg_time
        print "Distancia"
    return val


if __name__ == '__main__':
    import sys
    sys.setrecursionlimit(200000)
    by_word = False
    if len(sys.argv) >= 4 and sys.argv[3] == "word":
        by_word = True
    print main(sys.argv[1], sys.argv[2], by_word=by_word, is_file=True)
