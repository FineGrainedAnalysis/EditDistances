import re
import string

def clean_text(filename):
    new_filename = filename.replace(".","_clean.")
    with open(filename, 'r') as myfile:
        data=re.sub(r"\s+"," ",myfile.read())
    data = data.lower()
    data = data.translate(None, string.punctuation)
    file = open(new_filename,"w") 
    file.write(data) 
    file.close() 

def main(filename):
    clean_text(filename)

if __name__ == '__main__':
    import sys
    main(sys.argv[1])