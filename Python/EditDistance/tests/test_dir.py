import sys
sys.path.append('..')
from classic_dir import main as cdir
from adaptive_dir import main as adir
from random_string import generate_test_case, string_generator
import numpy as np
import unittest


class TestDeleteInsert(unittest.TestCase):
    # Test classical version

    def test_full_delete_c(self):
        self.assertEqual(cdir("aaaaa", ""), 5)
        self.assertEqual(cdir("baaaa", "b"), 4)
        self.assertEqual(cdir("aaaab", "a"), 4)
        self.assertEqual(cdir("aabaa", "b"), 4)

    def test_full_insert_c(self):
        self.assertEqual(cdir("", "aaaaa"), 5)
        self.assertEqual(cdir("b", "baaaa"), 4)
        self.assertEqual(cdir("b", "aaaab"), 4)
        self.assertEqual(cdir("b", "aabaa"), 4)

    def test_full_replace_c(self):
        self.assertEqual(cdir("bar", "car"), 1)
        self.assertEqual(cdir("cat", "cut"), 1)
        self.assertEqual(cdir("aaaaaaaa", "bacadafa"), 4)

    def test_basic_delete_insert_replace_c(self):
        self.assertEqual(cdir("kitten", "sitting"), 3)
        self.assertEqual(cdir("geek", "gesek"), 1)
        self.assertEqual(cdir("sunday", "saturday"), 3)

    def test_by_word_c(self):
        self.assertEqual(cdir("ki tt en", "si tt in g", by_word=True), 3)
        self.assertEqual(cdir("ge ek", "ge s ek", by_word=True), 1)
        self.assertEqual(cdir("su n day", "sa tur day", by_word=True), 2)

    def test_random_string_c(self):
        for i in range(0, 7):
            string = string_generator(2**i)
            tmp_arr = np.random.randint(0, 10, size=(2**i+1))
            new_string, dist = generate_test_case(string, tmp_arr)
            self.assertEqual(cdir(string, new_string), dist)

    # Test adaptive version
    
    def test_full_delete_a(self):
        self.assertEqual(adir("aaaaa", ""), 5)
        self.assertEqual(adir("baaaa", "b"), 4)
        self.assertEqual(adir("aaaab", "a"), 4)
        self.assertEqual(adir("aabaa", "b"), 4)

    def test_full_insert_a(self):
        self.assertEqual(adir("", "aaaaa"), 5)
        self.assertEqual(adir("b", "baaaa"), 4)
        self.assertEqual(adir("b", "aaaab"), 4)
        self.assertEqual(adir("b", "aabaa"), 4)

    def test_full_replace_a(self):
        self.assertEqual(adir("cat", "cut"), 1)
        self.assertEqual(adir("bar", "car"), 1)
        self.assertEqual(adir("aaaaaaaa", "bacadafa"), 4)

    def test_basic_delete_insert_a(self):
        self.assertEqual(adir("kitten", "sitting"), 3)
        self.assertEqual(adir("geek", "gesek"), 1)
        self.assertEqual(adir("sunday", "saturday"), 3)

    def test_by_word_a(self):
        self.assertEqual(adir("ki tt en", "si tt in g", by_word=True), 3)
        self.assertEqual(adir("ge ek", "ge s ek", by_word=True), 1)
        self.assertEqual(adir("su n day", "sa tur day", by_word=True), 2)

    def test_random_string_a(self):
        for i in range(0, 7):
            string = string_generator(2**i)
            tmp_arr = np.random.randint(0, 10, size=(2**i+1))
            new_string, dist = generate_test_case(string, tmp_arr)
            self.assertEqual(adir(string, new_string), dist)

    def test_versus(self):
        for i in range(0, 10):
            string1 = string_generator(np.random.randint(0, 2**i))
            string2 = string_generator(np.random.randint(0, 2**i))
            self.assertEqual(cdir(string1, string2), adir(string1, string2))

    
if __name__ == '__main__':
    unittest.main()
