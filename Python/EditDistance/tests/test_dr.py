import sys
sys.path.append('..')
from classic_dr import main as cdr
from adaptive_dr import main as adr
from random_string import string_generator
import numpy as np
import unittest


class TestDeleteInsert(unittest.TestCase):
    # Test classical version

    def test_full_delete_c(self):
        self.assertEqual(cdr("aaaaa", ""), 5)
        self.assertEqual(cdr("baaaa", "b"), 4)
        self.assertEqual(cdr("aaaab", "a"), 4)
        self.assertEqual(cdr("aabaa", "b"), 4)

    def test_full_insert_c(self):
        self.assertEqual(cdr("", "aaaaa"), -1)
        self.assertEqual(cdr("b", "baaaa"), -1)
        self.assertEqual(cdr("b", "aaaab"), -1)
        self.assertEqual(cdr("b", "aabaa"), -1)

    def test_full_replace_c(self):
        self.assertEqual(cdr("bar", "car"), 1)
        self.assertEqual(cdr("cat", "cut"), 1)
        self.assertEqual(cdr("aaaaaaaa", "bacadafa"), 4)

    def test_basic_delete_replace_c(self):
        self.assertEqual(cdr("kittens", "sitting"), 3)
        self.assertEqual(cdr("greek", "gesek"), 2)
        self.assertEqual(cdr("saturday", "sunday"), 3)
    
    def test_by_word_c(self):
        self.assertEqual(cdr("ki tt en s", "si tt in g", by_word=True), 3)
        self.assertEqual(cdr("gr e ek", "ge s ek", by_word=True), 2)
        self.assertEqual(cdr("sa tur day", "su n day", by_word=True), 2)

    # Test adaptive version

    def test_full_delete_a(self):
        self.assertEqual(adr("aaaaa", ""), 5)
        self.assertEqual(adr("baaaa", "b"), 4)
        self.assertEqual(adr("aaaab", "a"), 4)
        self.assertEqual(adr("aabaa", "b"), 4)

    def test_full_insert_a(self):
        self.assertEqual(adr("", "aaaaa"), -1)
        self.assertEqual(adr("b", "baaaa"), -1)
        self.assertEqual(adr("b", "aaaab"), -1)
        self.assertEqual(adr("b", "aabaa"), -1)

    def test_full_replace_a(self):
        self.assertEqual(adr("bar", "car"), 1)
        self.assertEqual(adr("cat", "cut"), 1)
        self.assertEqual(adr("aaaaaaaa", "bacadafa"), 4)

    def test_basic_delete_replace_a(self):
        self.assertEqual(adr("kittens", "sitting"), 3)
        self.assertEqual(adr("greek", "gesek"), 2)
        self.assertEqual(adr("saturday", "sunday"), 3)

    def test_by_word_a(self):
        self.assertEqual(adr("ki tt en s", "si tt in g", by_word=True), 3)
        self.assertEqual(adr("gr e ek", "ge s ek", by_word=True), 2)
        self.assertEqual(adr("sa tur day", "su n day", by_word=True), 2)

    def test_versus(self):
        for i in range(0, 15):
            random_val = np.random.randint(0, 2**i)
            string1 = string_generator(random_val)
            string2 = string_generator(np.random.randint(random_val, 2**i+1))
            self.assertEqual(cdr(string1, string2), adr(string1, string2))


if __name__ == '__main__':
    unittest.main()
