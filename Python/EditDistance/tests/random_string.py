import string
import random
from collections import defaultdict
chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
def string_generator(size, chars= chars):
     return ''.join(random.choice(chars) for _ in range(size))

def get_nonincluded_chars(string, chars=chars ):
    unique_chars = ''.join(defaultdict.fromkeys(string).keys())
    uc_set = set(unique_chars)
    charset = set(chars)
    return ''.join(charset-uc_set)

def generate_test_case(string, array):
    if len(string) != len(array)-1:
        print "Array size does not match with string length"
        exit(0)
    chars = get_nonincluded_chars(string)
    new_string = ""
    distance = 0
    for i in range(0,len(array)-1):
        distance += array[i]
        random_str = string_generator(array[i], chars)
        new_string += random_str+string[i]
    new_string += string_generator(array[len(array)-1], chars)
    distance += array[len(array)-1]
    return new_string, distance
