import sys
sys.path.append('..')
from classic_di import main as cdi
from adaptive_di import main as adi
from random_string import generate_test_case, string_generator
import numpy as np
import unittest


class TestDeleteInsert(unittest.TestCase):
    # Test classical version

    def test_full_delete_c(self):
        self.assertEqual(cdi("aaaaa", ""), 5)
        self.assertEqual(cdi("baaaa", "b"), 4)
        self.assertEqual(cdi("aaaab", "a"), 4)
        self.assertEqual(cdi("aabaa", "b"), 4)

    def test_full_insert_c(self):
        self.assertEqual(cdi("", "aaaaa"), 5)
        self.assertEqual(cdi("b", "baaaa"), 4)
        self.assertEqual(cdi("b", "aaaab"), 4)
        self.assertEqual(cdi("b", "aabaa"), 4)

    def test_basic_delete_insert_c(self):
        self.assertEqual(cdi("cat", "cut"), 2)
        self.assertEqual(cdi("kitten", "sitting"), 5)
        self.assertEqual(cdi("geek", "gesek"), 1)
        self.assertEqual(cdi("sunday", "saturday"), 4)

    def test_random_string_c(self):
        for i in range(0, 7):
            string = string_generator(2**i)
            tmp_arr = np.random.randint(0, 10, size=(2**i+1))
            new_string, dist = generate_test_case(string, tmp_arr)
            self.assertEqual(cdi(string, new_string), dist)

    def test_by_word_c(self):
        self.assertEqual(cdi("c a t","c u t",by_word=True), 2)
        self.assertEqual(cdi("ki tt en", "si tt in g", by_word=True), 5)
        self.assertEqual(cdi("ge ek", "ge s ek", by_word=True), 1)
        self.assertEqual(cdi("su n day", "sa tur day", by_word=True), 4)

    # Test adaptive version

    def test_full_delete_a(self):
        self.assertEqual(adi("aaaaa", ""), 5)
        self.assertEqual(adi("baaaa", "b"), 4)
        self.assertEqual(adi("aaaab", "a"), 4)
        self.assertEqual(adi("aabaa", "b"), 4)

    def test_full_insert_a(self):
        self.assertEqual(adi("", "aaaaa"), 5)
        self.assertEqual(adi("b", "baaaa"), 4)
        self.assertEqual(adi("b", "aaaab"), 4)
        self.assertEqual(adi("b", "aabaa"), 4)

    def test_basic_delete_insert_a(self):
        self.assertEqual(adi("cat", "cut"), 2)
        self.assertEqual(adi("kitten", "sitting"), 5)
        self.assertEqual(adi("geek", "gesek"), 1)
        self.assertEqual(adi("sunday", "saturday"), 4)

    def test_random_string_a(self):
        for i in range(0, 7):
            string = string_generator(2**i)
            tmp_arr = np.random.randint(0, 10, size=(2**i+1))
            new_string, dist = generate_test_case(string, tmp_arr)
            self.assertEqual(adi(string, new_string), dist)

    def test_by_word_a(self):
        self.assertEqual(adi("c a t","c u t",by_word=True), 2)
        self.assertEqual(adi("ki tt en", "si tt in g", by_word=True), 5)
        self.assertEqual(adi("ge ek", "ge s ek", by_word=True), 1)
        self.assertEqual(adi("su n day", "sa tur day", by_word=True), 4)

    def test_versus(self):
        for i in range(0,10):
            string1 = string_generator(np.random.randint(0,2**i)) 
            string2 = string_generator(np.random.randint(0,2**i))
            self.assertEqual(cdi(string1,string2),adi(string1,string2))



if __name__ == '__main__':
    unittest.main()
