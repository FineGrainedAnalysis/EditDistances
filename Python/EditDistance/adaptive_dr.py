from ed_algorithms.DPBase import DPBase
from ed_algorithms.StringClassic import StringClassic
from ed_algorithms.WordJump import WordJump
from ed_algorithms.IndexingTools import IndexingTools
import time


def adaptive_delete_replace(base, i, j, source, target):
    val = 0
    base.increase_counter()
    if i < j:
        return source.len()+target.len()+1

    elif j < 0:
        return i+1

    if base.get(i, j) < 0:
        s = source.get(i)
        t = target.get(j)
        rank_s = source.rank(t, i)
        replace_dist = 1 + \
            adaptive_delete_replace(base, i-1, j-1, source, target)
        if s == t:
            val = replace_dist-1
        elif rank_s == 0:
            val = replace_dist
        else:
            select_s = source.select(t, rank_s)
            delete_dist = i - select_s + \
                adaptive_delete_replace(base, source.select(
                    t, rank_s)-1, j-1, source, target)
            val = min(delete_dist, replace_dist)
        base.put(i, j, val)
    return base.get(i, j)


def main(source, target, by_word=False, is_file=False):

    dp = DPBase(adaptive_delete_replace)
    s = None
    t = None
    if by_word:
        s = WordJump(source, is_file)
        t = WordJump(target, is_file)
    else:
        s = StringClassic(source, is_file)
        t = StringClassic(target, is_file)
    start = time.time()
    s.build_index()
    t.build_index()
    index_time = time.time()-start
    val = dp.run(s, t)
    alg_time = time.time()-start
    if is_file:
        print "Alfabeto s, t: ", len(s.get_alphabet()), len(t.get_alphabet())
        print "Tamano interseccion de alfabeto: ", len(
            (s.get_alphabet() & t.get_alphabet()))
        print "Numero de recursiones: ", dp.get_counter()
        print "Numero de rank: ", s.get_rank_counter()+t.get_rank_counter()
        print "Numero de select: ", s.get_select_counter()+t.get_select_counter()
        print "Tiempo de ejecucion indice: ", index_time
        print "Tiempo de algoritmo: ", alg_time
        print "Delta: ", dp.get_delta(), " / ", dp.get_max_delta() - \
            dp.dist.shape[1]
        print "Distancia"
    if val > s.len() + t.len():
        val = -1
    return val


if __name__ == '__main__':
    import sys
    sys.setrecursionlimit(200000)
    by_word = False
    if len(sys.argv) >= 4 and sys.argv[3] == "word":
        by_word = True
    print main(sys.argv[1], sys.argv[2], by_word=by_word, is_file=True)
