from collections import defaultdict
import numpy as np


class IndexingTools:
    rank_mat = None
    access = None
    rank_counter = 0
    select_counter = 0

    def __init__(self, string):
        mat, access = self.make_rank(string)
        self.rank_mat = mat
        self.access = access

    def get_rank_counter(self):
        return self.rank_counter

    def get_select_counter(self):
        return self.select_counter

    def make_rank(self, sjump):
        access = {}
        charset = set()
        n = sjump.len()
        for i in range(0, n):
            charset.add(sjump.get(i))
        chars = np.array(list(charset))
        c = len(chars)
        mat = np.zeros((c, n), dtype=int)
        for i in range(0, c):
            access[chars[i]] = i
        for i in range(0, n):
            char = access[sjump.get(i)]
            mat[char][i:] += 1
        return mat, access

    def rank(self, char, j):
        self.rank_counter += 1
        if self.access.has_key(char):
            char_num = self.access[char]
            return self.rank_mat[char_num][j]
        return 0

    def sequential_select(self, char, j):
        self.select_counter += 1
        _, n = self.rank_mat.shape
        char_num = self.access[char]
        for i in range(0, n):
            if self.rank_mat[char_num][i] == j:
                return i
        return -1

    def binary_search_select(self, char, j):
        self.select_counter += 1
        upper = self.rank_mat.shape[1]-1
        char_num = self.access[char]
        p = np.random.randint(upper)
        lower = 0
        val = 0
        i = -2
        while (lower <= upper):
            val = self.rank_mat[char_num][p]
            if val == j:
                i = p
                break
            elif val < j:
                lower = p+1
            else:
                upper = p-  1
            p = lower + (upper - lower)/2
                 
        while(val == j and i >= 0):
            i = i - 1
            val = self.rank_mat[char_num][i]
        i += 1
        return i

    def doubling_search_select(self, char, j):
        self.select_counter += 1
        upper = self.rank_mat.shape[1]-1
        char_num = self.access[char]
        i = 0
        val = 0
        while (j > val and 2**i < upper):
            val = self.rank_mat[char_num][2**i]
            i += 1

        if upper > 2**i:
            upper = 2**i
        lower = 2**(i-1)
        p = np.random.randint(lower, high=upper)
        i = -2
        while (lower <= upper):
            val = self.rank_mat[char_num][p]
            if val == j:
                i = p
                break
            elif val < j:
                lower = p+1
            else:
                upper = p-  1
            p = lower + (upper - lower)/2
                 
        while(val == j and i >= 0):
            i = i - 1
            val = self.rank_mat[char_num][i]
        i += 1
        return i
    