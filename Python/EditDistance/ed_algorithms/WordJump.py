from StringJump import StringJump
from IndexingTools import IndexingTools
from read_file import read_file
import numpy as np


class WordJump(StringJump):

    def __init__(self, string, is_file=False, spacing_char=" "):
        if is_file:
            string = read_file(string)
        super(WordJump, self).__init__(string)
        self.build_jump_index(spacing_char)
        self.calculate_len()

    def build_jump_index(self, spacing_char):
        array = [-1]
        for i, ch in enumerate(self.string):
            if ch == spacing_char:
                array.append(i)
        if array[len(array)-1] != len(self.string)-1:
            array.append(len(self.string))
        else:
            array[len(array)-1] += 1
        self.jump_index = np.zeros(len(array),dtype=int)
        for i in range(0, len(array)):
            self.jump_index[i] = array[i]

    def build_index(self):
        self.indexing = IndexingTools(self)

    def get_rank_counter(self):
        return self.indexing.get_rank_counter()

    def get_select_counter(self):
        return self.indexing.get_select_counter()

    def calculate_len(self):
        self.size = len(self.jump_index)-1

    def get(self, i):
        low = self.jump_index[i]+1
        hi = self.jump_index[i+1]
        return self.string[low:hi]
