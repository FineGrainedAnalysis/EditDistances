from StringJump import StringJump
from IndexingTools import IndexingTools
from read_file import read_file

class StringClassic(StringJump):
    
    def __init__(self, string, is_file=False):
        if is_file:
            string=read_file(string)
        super(StringClassic, self).__init__(string)
        self.calculate_len()

    def build_index(self):
        self.indexing = IndexingTools(self)

    def get_rank_counter(self):
        return self.indexing.get_rank_counter()

    def get_select_counter(self):
        return self.indexing.get_select_counter()

    def calculate_len(self):
        self.size = len(self.string)

    def get(self, i):
        return self.string[i]
        