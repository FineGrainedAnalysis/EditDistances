
class StringJump(object):
    string = ""
    size = -1
    indexing = None
    jump_index = None

    def __init__(self, string):
        self.string = string

    def build_index(self):
        pass

    def get_alphabet(self):
        charset = set()
        n = self.len()
        for i in range(0, n):
            charset.add(self.get(i))
        return charset

    def rank(self, char, j):
        return self.indexing.rank(char, j)

    def select(self, char, j):
        return self.indexing.binary_search_select(char, j)

    def build_jump_index(self):
        pass

    def calculate_len(self):
        pass

    def len(self):
        if self.size < 0:
            self.calculate_len()
        return self.size

    def get(self, i):
        pass
