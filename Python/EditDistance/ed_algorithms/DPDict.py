from DPBase import DPBase
from collections import defaultdict

def defaultvalue():
    return -1

class DPDict(DPBase):
    dictionary = None

    def __init__(self, fun):
        self.fun = fun
        self.dictionary = defaultdict(defaultvalue)

    def get(self, i, j):
        return self.dictionary[(i,j)]
            
    def put(self, i, j, value):
        self.dictionary[(i,j)] = value

    def run(self, source, target):
        n = source.len()
        m = target.len()
        distance = self.fun(self, n-1, m-1, source, target)
        return distance