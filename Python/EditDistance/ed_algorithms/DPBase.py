import numpy as np


class DPBase(object):
    dist = None
    access_counter = 0
    fun = None
    jump_distance = 0
    jumps = 0

    def __init__(self, fun):
        self.fun = fun
        self.access_counter = 0
        self.dist = None
        self.jumps = 0

    def get(self, i, j):
        if i < 0 or j < 0:
            return 0
        return self.dist[i][j]

    def increase_counter(self):
        self.access_counter += 1
    
    def jump(self):
        self.jumps += 1
    
    def get_delta(self):
        counter = 0
        i, j = self.dist.shape[0], self.dist.shape[1]
        for d in range(-i+1,j):
            if np.any(self.dist.diagonal(d) >= 0):
                counter += 1
        return counter

    def get_max_delta(self):
        return self.dist.shape[0]+self.dist.shape[1]-1

    def get_counter(self):
        return self.access_counter

    def put(self, i, j, value):
        self.dist[i][j] = value

    def run(self, source, target):
        n = source.len()
        m = target.len()
        self.dist = -np.ones((n, m), dtype='i4')
        distance = self.fun(self, n-1, m-1, source, target)
        return distance
