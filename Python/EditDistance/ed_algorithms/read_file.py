import os

def read_file(filename):
    string = ""
    myfile = open(filename, 'r')
    try:
        FILE_SIZE = int(os.environ['FILE_SIZE'])
        string = myfile.read(FILE_SIZE)

    except KeyError:
        string = myfile.read()
    return string
