from ed_algorithms.DPBase import DPBase
from ed_algorithms.WordJump import WordJump
from ed_algorithms.StringClassic import StringClassic
import time

def classic_delete_insert_replace(base, i, j, source, target):
    val = 0
    base.increase_counter()
    if j < 0:
        return i+1
    elif i < 0:
        return j+1
    if base.get(i, j) < 0:
        if source.get(i) == target.get(j):
            val = classic_delete_insert_replace(base, i-1, j-1, source, target)
        else:
            delete_dist = classic_delete_insert_replace(
                base, i-1, j, source, target)
            insert_dist = classic_delete_insert_replace(
                base, i, j-1, source, target)
            replace_dist = classic_delete_insert_replace(
                base, i-1, j-1, source, target)
            val = 1 + min(insert_dist, delete_dist, replace_dist)
        base.put(i, j, val)
    return base.get(i, j)


def base_case(base, i, j):
    if i < 0:
        return j+1
    return i+1


def classic_delete_insert_replace_iterative(base, n, m, source, target):
    for i in range(0, n+1):
        for j in range(0, m+1):
            if source.get(i) == target.get(j):
                val = base.get(i-1, j-1)
            else:
                delete_dist = base.get(i-1, j)
                insert_dist = base.get(i, j-1)
                replace_dist = base.get(i-1, j-1)
                val = 1 + min(insert_dist, delete_dist, replace_dist)
            base.put(i, j, val)
    return base.get(n, m)


def main(source, target, by_word=False, is_file=False):

    dp = DPBase(classic_delete_insert_replace)
    s = None
    t = None
    if by_word:
        s = WordJump(source, is_file)
        t = WordJump(target, is_file)
    else:
        s = StringClassic(source, is_file)
        t = StringClassic(target, is_file)
    start = time.time()   
    val = dp.run(s, t)
    alg_time = time.time()-start
    if is_file:
        print "Numero de recursiones: ", dp.get_counter()
        print "Tiempo de algoritmo: ", alg_time
        print "Tamano interseccion de alfabeto: ", len((s.get_alphabet() & t.get_alphabet()))
        print "Distancia"
    return val


if __name__ == '__main__':
    import sys
    sys.setrecursionlimit(200000)
    by_word = False
    if len(sys.argv) >= 4 and sys.argv[3] == "word":
        by_word = True
    print main(sys.argv[1], sys.argv[2], by_word=by_word, is_file=True)