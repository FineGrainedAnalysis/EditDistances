\documentclass[usletter]{llncs}
\usepackage{makeidx}  % allows for indexgeneration

\usepackage[T1]{fontenc}
\usepackage{times} % Specify font, especially when using ps2pdf after.
\usepackage{pifont}
\usepackage{epsfig}
\usepackage{algorithmic}%% For the Algorithms.
\usepackage{algorithm} %% For the floating Algorithm environment.
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[sort]{natbib} % To have the references sorted in good order.

\usepackage{fullpage}

\usepackage{version}
\excludeversion{INUTILE}
\excludeversion{LONG}
\excludeversion{TODO}


% Do away with "club lines" (the first line of a paragraph on the last line of a page; also called "orphan lines") 
% and "widow lines" (the last line of the paragraph on a new page): 
\widowpenalty=10000   
\clubpenalty=10000
\flushbottom

% Always aknowledge sources and pointers and proof readers
\newenvironment{acknowledgments}{\vspace{.4cm}{\noindent \em Acknowledgements:}}{}%

\begin{document}
 
\pagestyle{headings}  % switches on printing of running heads
% \addtocmark{        \TitleOfMyWork            }

\mainmatter              % start of the contributions
\title{   Indexed Dynamic Programming:\\ Simple Edit Distances}
% \titlerunning{      TITLE OF MY WORK            } 
%
\author{J\'er\'emy Barbay}
% \authorrunning{J\'er\'emy Barbay}   % abbreviated author list (for running head)
%
%%%% modified list of authors for the TOC (add the affiliations)
\tocauthor{J\'er\'emy Barbay (Universidad de Chile)}
%
\institute{
%
  Departmento de Ciencias de la Computaci{\'o}n, \\
  University of Chile, \\
\email{jeremy@barbay.cl}
}

\maketitle              % typeset the title of the contribution

\begin{abstract}
There are efficient dynamic programming solutions to the computation of the edit distance from $S\in[1..\sigma]^n$ to $T\in[1..\sigma]^m$, for many natural subsets of edit operations, typically in time within $O(nm)$ in the worst-case over strings of respective lengths $n$ and $m$, and in time within $O(n{+}m)$ in some of the best cases (e.g. disjoint alphabets). We provide a finer analysis of the computational complexity of the instances between those two extremes, for a variety of subset of edit operations. Our analysis techniques can be extended to other dynamic programming solutions. 
\end{abstract}

\begin{center}
  \begin{minipage}{.9\textwidth}
    \noindent{\bf Keywords:} 
Adaptive (analysis of) Algorithms,
Deletion,
Edit Distance, 
Parameterized Analysis,
Insertion,
Rewrite,
Swap
  \end{minipage}
\end{center}

\section{Introduction}

% \subsection{Context}
Finding the (correction) distance from a source string $S$ to a target string $T$ is a fundamental problem in Computer Science, with a wide range of applications, from text processing and information retrieval to computational biology. The typical string distance between two strings is defined by the minimum number of \texttt{insertions}, \texttt{deletions} (in both cases, of a character at an arbitrary position of $S$) and \texttt{rewrites} (of one character of $S$ by some other) needed to transform the string $S$ into $T$. Many generalizations have been defined in the literature, including weighted costs for the edit operations, and different sets of edit operations -- the standard set is \{\texttt{insertion},\texttt{deletion},\texttt{rewrite}\}.

% \subsection{Previous works}
There are efficient dynamic programming solutions to the computation of the distance from $S$ to $T$ for many natural subsets of edit operations, typically working in time within $O(nm)$ worst-case time, with $n = |S|$ the length of $S$, and $m = |T|$ the lenght of $T$. 
\begin{TODO}
DEVELOP Biblio about nm algorithm here, copy pasting from TALG paper
\end{TODO}

A notable exception is the \textsc{Insert-Swap Correction Distance}, the minimum number of edit operations to transform $S$ into $T$ when only two edit operations, namely, the usual \texttt{insert} operation and \texttt{swap}, exchanging two adjacent symbols, are available. This distance (as well as its symetric counter part, the \textsc{Delete-Swap Correction Distance}) is NP-hard to compute, but categories of instances easier than the worst case have been considered:
%
Abukhzam~et al.~\cite{2011-DO-ChargeAndReduceAFixedParameterAlgorithmForStringToStringCorrection-AbukhzamFernauLangstonLeeculturaStege} proposed an algorithm running in exponential time adaptive to the output distance $d$ for general alphabets;
%
Meister~\cite{2015-TCS-UsingSwapsAndDeletesToMakeStringsMatch-Meister} proposed a polynomial time algorithm for finite alphabets; and
%
Barbay and Perez-Lantero~\cite{2015-SPIRE-AdaptiveComputationOfTheSwapInsertCorrectionDistance-BarbayPerez} proposed a 
 new algorithm for the case of strings built from symbols in a finite size alphabet. The new algorithm’s worst-case time is in $O(dm + d2 nmg d−1 )$, where $g$ is the imbalance of the input instance, and  measures, roughly speaking, how unbalanced are the frequency counts of the symbols of the alphabet in the given strings. The parameter $g$ can be as high as $n$, but in many instances the value of $g$ will be low (even $0$) and the algorithm will run very fast. 
 \begin{LONG}
The algorithm itself results from a clever application of dynamic programming, yet simple enough to be easily implemented and practical. One of the most salient features of the paper is the adaptive analysis of the algorithm, rather than focusing in the worst-case instances of sizes n and m. The authors show how the complexity of the algorithm is affected by the parameter g, which becomes thus a measure of the difficulty of the given instance. Moreover the value g can be easily computed for any two given strings n and m. There is some interplay between the analysis and the algorithm design, as certain features of the algorithm are designed the way they are to exploit as much as possible the situations where the imbalance is low. In this case, adaptive analysis spots the difficult instances and suggests how to take advantadge of the easy instances.  In the next section of this report I will discuss in more detail some suggestions for changes which could improve the presentation and I list a few minor errors, etc. Here, I will conclude this section on general comments with some final remarks and my overall recomendation.

Barbay and Perez-Lantero~\cite{2015-SPIRE-AdaptiveComputationOfTheSwapInsertCorrectionDistance-BarbayPerez}  present an interesting algorithm based on well known and well understood principles (dynamic programming) using very clean and clever algorithmic ideas to make the best of it. The algorithm yields a substantial improvement on the complexity of the computation of the Swap-Insert distance, for which only exponential time algorithms are known in the general case, and polynomial time (but of high degree) algorithms are available for the important case of finite size alphabets. To be more precise the authors quantify, thanks to the adaptive analysis, how much faster their algorithm can be when the “difficulty” parameter g is low—a situation arising often enough in practice; when the “difficulty” parameter g is high, the algorithm’s complexity is given by a polynomial in n and m of high degree in d. Still, even in that worst-case scenario the new algorithm improves Meister’s algorithm as the degree of the polynomial in Meister’s algorithm is larger.  Besides the intrinsic interest and significance of the algorithmic side of the paper, I think the paper is also a good contribution to the Transactions on Algorithms because of the careful and delicate adaptive analysis that the authors carry out. In my view this is a very interesting part of the paper which goes beyond the specific application to the computation of the Swap-Insert distance.
 \end{LONG}


% \subsection{Contributions}

 Given the positive results of a fine analysis of the computation of the \textsc{Insert-Swap Correction Distance} and \textsc{Delete-Swap Correction Distance}, \textbf{can the computation of other edit distances}, even if their computational complexity in the worst case is ``only'' polynomial, \textbf{be similarly refined}, so that to identify large classes of instances for which the edit distance can be computed faster than in the worst case over instances of similar size?

We answer positively for all edit distances based on subsets of the set of operations \{\texttt{insert},\texttt{delete},\texttt{rewrite}\}.

\section{Definitions}


\section{Algorithm}
\subsection{Doing things}
\begin{algorithm}[ht!]
\caption{{\tt }}
\label{alg:}
\medskip\hrule
\begin{minipage}{.49\textwidth}
\begin{algorithmic}
\WHILE{ }
  \STATE ;
\ENDWHILE  
\STATE {\bf return};
\end{algorithmic}
\end{minipage}
\end{algorithm}
%
\subsection{An example of Execution}
\subsection{Analysis}
\subsection{Optimality}

\section{Discussion}
% \subsubsection*{In this paper,}
% \subsubsection*{This is important,}
% \subsubsection*{Perspectives:}


\medskip 
\textbf{Acknowledgments:}
The author would like to thanks profusely 
Pablo P\'erez-Lantero for introducing the problem and dynamic programming;
Felipe Lizama for a semester of very interesting discussions about this approach; and
an anonymous referee from Transaction on Algorithms for his very positive feedback and encouragements.
% 
\textbf{Funding:} J\'er\'emy Barbay is partially funded by 
the project Fondecyt Regular no 1170366 from Conicyt, Chile.
%the Millennium Nucleus RC130003 ``Information and Coordination in Networks''.
%
% \textbf{Author Contributions:} 
%
% \textbf{Competing Interests:} The authors declare that they have no competing financial interests relevant to the material exposed in this article.
%
% \textbf{Data and Material Availability:}

%%%%%%%%  BIBLIO  %%%%%%%%%%%%
%\newpage
\bibliographystyle{abbrv} % see https://www.sharelatex.com/learn/Bibtex_bibliography_styles to choose your bibliography style
\bibliography{/home/jbarbay/EverGoing/Bibliography/bibliographyDatabaseJeremyBarbay,/home/jbarbay/EverGoing/Publications/publications-ExportedFromOrgmode-Barbay}





\end{document}

















%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
