\documentclass{llncs}
\usepackage[T1]{fontenc}
\usepackage{times} % Specify font, especially when using ps2pdf after.
\usepackage{pifont}
\usepackage{epsfig}
\usepackage{algorithmic}%% For the Algorithms.
\usepackage{algorithm} %% For the floating Algorithm environment.
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[sort]{natbib} % To have the references sorted in good order.
\usepackage{fullpage}
\usepackage{jeremy}
\usepackage{versions}
\excludeversion{INUTILE}
\excludeversion{LONG}


% Always aknowledge sources and pointers and proof readers
\newenvironment{acknowledgments}{\vspace{.4cm}{\noindent \em Acknowledgements:}}{}%

\begin{document}

\pagestyle{headings}  % switches on printing of running heads
\addtocmark{                    }

\mainmatter              % start of the contributions
\newcommand{\myTitle}{Block Edit Distances}
\title{ {\small Research Proposal:} \\ \emph{\myTitle}   }
\titlerunning{\myTitle } 
%
\author{J\'er\'emy Barbay\inst{1} \and Travis Gagie\inst{2}}
\authorrunning{J. Barbay \and T. Gagie}   % abbreviated author list (for running head)
%
%%%% modified list of authors for the TOC (add the affiliations)
\tocauthor{J\'er\'emy Barbay (Universdad de Chile)
Travis Gagie (Universidad Diego Portales)}
%
\institute{
%
%  Departmento de Ciencias de la Computaci{\'o}n, \\
  University of Chile, \email{jeremy@barbay.cl}
\and 
Universidad Diego Portales, \email{travis.gagie@gmail.com}
}

\maketitle              % typeset the title of the contribution

\begin{abstract}
The edit distance from a string $S$ of length $n$ to another string $T$ of length $m$ on the alphabet $[1..d]$ is the minimum number of operations transforming $S$ into $T$.
It has been well studied for sets of operators acting on unique symbols, but much less so on sets of operators or blocks of symbols, whereas such "block edit distances" would have great application in the analysis of genomes in bio informatics and in difference computations in  control version systems, where entire blocks of symbols can be deleted, duplicated and moved.
We propose to design and analize algorithms for various subset of operators from the set formed by \texttt{Block Deletion}, \texttt{Block Duplication}, \texttt{Block Move} and \texttt{Block Insertion}, where traditional operations on unique symbols can be seen as block operations on a block of unitary size.  
\end{abstract}


\begin{center}
  \begin{minipage}{.9\textwidth}
    \noindent{\bf Keywords:} % (One per line)
Algorithms, 
Bio Informatics, 
Control version system,
Edit Distance.
  \end{minipage}
\end{center}


\subsection*{Introduction}

Given a source string $S\in[1..\sigma]^n$ and a target string $T\in[1..\sigma]^m$ and a list of correction operations on strings, the =Edit distance= is the minimum number of operations required to transform the string~$S$ into the string~$T$. Introduced in 1974 by Wagner and Fischer~\cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer}, this concept has many applications, from suggesting corrections for typing mistakes, to decomposing the changes between two consecutive versions into a minimum number of correction steps, for example within a control version syste such as {\tt cvs}, {\tt svn} or {\tt git}. Each distinct set of correction operators yields a distinct correction distance on strings, with a distinct computational cost in the worst case over strings of lenght bounded by $n$ and $m$.

Wagner and Fischer~\cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer} originally considered the following set of three operations: the \texttt{insertion} of a symbol at some arbitrary position; the \texttt{deletion} of a symbol at some arbitrary position; and the \texttt{substitution} of a symbol at some arbitrary position. Wagner~\cite{1975-JACM-AnExtensionOfTheStringToStringCorrectionProblem-WagnerLowrance} added to this set the \texttt{swap} operator, which exchanges the positions of two contiguous symbols.  Out of the $2^4-1=15$ non-empty subsets of this set of operations, the correction distance can be computed in time polynomial in $n$ and $m$ for 13 of those, with the notable exception of two distances, the \textsc{Swap-Insert Correction} distance and its symmetric the \textsc{Swap-Delete Correction} distance, which are equivalent by symmetry (see Figure \ref{fig:overview} for a summary). 

Those results are optimal in the worst case over instances of fixed lenghts $n$ and $m$: Wagner~\cite{1975-STOC-OnTheComplexityOfTheExtendedStringToStringCorrectionProblem-Wagner} showed in 1975 that the \textsc{Swap-Insert Correction} and \textsc{Swap-Delete Correction} distances are NP-hard to compute, and Backurs and Indyk~\cite{2015-STOC-EditDistanceCannotBeComputedInStronglySubquadraticTimeUnlessSETHIsFalse-BackursIndyk} showed in 2015 that the $O(n^2)$ upper bound for the computation of the \textsc{Insert-Delete-Substitute Correction} distance is optimal unless the \emph{Strong Exponential Time Hypothesis} (SETH) is false.

For this set of operators, the problem could have been considered closed, if not for the observation that each correction distance can be ``computed'' in linear time in various cases, such as when the two strings are identical (i.e. the distance is zero), or when the two strings are on disjoint alphabets (i.e. the distance is infinite or equal to $n{+}m$ depending of the set of operators considered). Developping further this line of work, Abu-Khzam et al.~\cite{2011-DO-ChargeAndReduceAFixedParameterAlgorithmForStringToStringCorrection-AbukhzamFernauLangstonLeeculturaStege} described an algorithm computing the \textsc{Insert-Swap Correction} distance $d$ from $S$ to $T$ in time within $O({1.6181}^d m)$, so that the computation is polynomial in $n$ and $m$ when the distance is finite\footnote{As the \textsc{Insert-Swap Correction} distance is infinite if $n>m$, which can be checked in linear time, the analysis needs consider only the case where $n\leq m$.}; and Barbay and Perez~\cite{2015-SPIRE-AdaptiveComputationOfTheSwapInsertCorrectionDistance-BarbayPerez}, building upon Meister's previous results~\cite{2015-TCS-UsingSwapsAndDeletesToMakeStringsMatch-Meister}, described an algorithm computing this distance in time within $O(\sigma^2nm\gamma^{\sigma-1})$ in the worst case over instances where $\sigma,n,m$ and $\gamma$ are fixed, where $\gamma$ measures a form of imbalance between the frequency distributions of each string.

\begin{figure}
\centering
\begin{tabular}{p{3cm}|p{2.5cm}|p{2cm}|p{8cm}}
  Set of Operators                             & Worst Case for fixed size & Multi Variate Results                                & References                                                                                                                                                                                                                                                     \\ 
\hline\hline
Deletion                                & $O(m)$                        &                                                     & \cite{2013-Master-TheBinaryStringtoStringCorrectionProblem-Spreen}                                                                                                                                                                                             \\
Insertion                               & $O(n)$                        &                                                     & \cite{2013-Master-TheBinaryStringtoStringCorrectionProblem-Spreen}                                                                                                                                                                                             \\
Substitition                            & $O(n)$                        &                                                     & \cite{2013-Master-TheBinaryStringtoStringCorrectionProblem-Spreen}                                                                                                                                                                                             \\
Swap                                    & $O(n^2)$                      &                                                     & \cite{2013-Master-TheBinaryStringtoStringCorrectionProblem-Spreen}                                                                                                                                                                                             \\
\hline\hline
Deletion, Insertion                     & $O(nm)$                       &                                                     & \cite{2000-SPIRE-ASurveyOfLongestCommonSubsequenceAlgorithms-BergrothHakonenRaita}                                                                                                                                                                             \\
Deletion, Substitution                  & $O(nm)$                       &                                                     & \cite{1975-STOC-OnTheComplexityOfTheExtendedStringToStringCorrectionProblem-Wagner}                                                                                                                                                                            \\
  Deletion, Swap                        & NP-complete                   & $O({1.6181}^d m)$, $O(\sigma^2nm\gamma^{\sigma-1})$ & \cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer,2011-DO-ChargeAndReduceAFixedParameterAlgorithmForStringToStringCorrection-AbukhzamFernauLangstonLeeculturaStege,2015-SPIRE-AdaptiveComputationOfTheSwapInsertCorrectionDistance-BarbayPerez} \\
Insertion, Substitution                 & $O(nm)$                       &                                                     & \cite{1975-STOC-OnTheComplexityOfTheExtendedStringToStringCorrectionProblem-Wagner}                                                                                                                                                                            \\
Insertion, Swap                         & NP-complete                   & $O({1.6181}^d m)$, $O(\sigma^2nm\gamma^{\sigma-1})$ & \cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer,2011-DO-ChargeAndReduceAFixedParameterAlgorithmForStringToStringCorrection-AbukhzamFernauLangstonLeeculturaStege,2015-SPIRE-AdaptiveComputationOfTheSwapInsertCorrectionDistance-BarbayPerez} \\
Substitution, Swap                      & $O(nm)$                       &                                                     & \cite{1975-STOC-OnTheComplexityOfTheExtendedStringToStringCorrectionProblem-Wagner}                                                                                                                                                                            \\
\hline\hline
Deletion, Insertion, Substitution       & $O(nm)$                       &                                                     & \cite{2000-SPIRE-ASurveyOfLongestCommonSubsequenceAlgorithms-BergrothHakonenRaita}                                                                                                                                                                             \\
Deletion, Insertion, Swap               & $O(nm)$                       &                                                     & \cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer}                                                                                                                                                                                              \\
Deletion, Substitution, Swap            & $O(nm)$                       &                                                     & \cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer}                                                                                                                                                                                              \\
Insertion, Substitution, Swap           & $O(nm)$                       &                                                     & \cite{1974-JACM-TheStringToStringCorrectionProblem-WagnerFischer}                                                                                                                                                                                              \\
\hline\hline
Deletion, Insertion, Substitution, Swap & $O(nm)$                       &                                                     & \cite{2000-SPIRE-ASurveyOfLongestCommonSubsequenceAlgorithms-BergrothHakonenRaita}                                                                                                                                                                             \\
\end{tabular}
\caption{Summary of previous results for various combinations of operators from the basic set \{\texttt{Insertion}, \texttt{Deletion}, \texttt{Substitution}, \texttt{Swap}\}.  The column labeled ``Worst Case for fixed size'' present results in the worst case over instances of fixed sizes $n$ and $m$, while the column labeled ``Multi Variate Results'' present results where the analysis was refined by various parameters, defined in the text. }
\label{fig:overview}
\end{figure}



\subsection*{Hypothesis}
\label{sec:hypothesis}

\subsection*{Objectives}



%%%%%%%%  BIBLIO  %%%%%%%%%%%%
%\newpage
\bibliographystyle{apalike} % see https://www.sharelatex.com/learn/Bibtex_bibliography_styles to choose your bibliography stile
\bibliography{/home/jbarbay/EverGoing/Bibliography/bibliographyDatabaseJeremyBarbay,/home/jbarbay/EverGoing/Publications/publications-ExportedFromOrgmode-Barbay}


% \appendix

% \section{Preliminary Results}
% \label{sec:preliminaryResults}



\end{document}

















%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
